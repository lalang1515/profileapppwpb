/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type {Node} from 'react';

import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import Icon from "react-native-vector-icons/FontAwesome";

/* $FlowFixMe[missing-local-annot] The type annotation(s) required by Flow's
 * LTI update could not be added via codemod */
const Section = ({children, title}): Node => {
  const isDarkMode = useColorScheme() === 'dark';
  return (
    <View style={styles.sectionContainer}>
      <Text
        style={[
          styles.sectionTitle,
          {
            color: isDarkMode ? Colors.white : Colors.black,
          },
        ]}>
        {title}
      </Text>
      <Text
        style={[
          styles.sectionDescription,
          {
            color: isDarkMode ? Colors.light : Colors.dark,
          },
        ]}>
        {children}
      </Text>
    </View>
  );
};

const data = [
  {nama: 'Ilalang'},
  {nama: 'Ilalang1'},
  {nama: 'Ilalang2'},
  {nama: 'Ilalang3'},
]

const App: () => Node = () => {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  return (
    <SafeAreaView style={backgroundStyle}>
      <StatusBar
        barStyle={isDarkMode ? 'light-content' : 'dark-content'}
        backgroundColor={backgroundStyle.backgroundColor}
      />
      <ScrollView contentInsetAdjustmentBehavior="automatic">
        <View style={{alignItems: "center", paddingTop: 50, paddingBottom: 30, backgroundColor: '#262626'}}>
          <View style={{width: 200, height: 200, borderRadius: 50, borderWidth: 3, borderColor: 'white', overflow: 'hidden', backgroundColor: '#000000'}}>
            <Image source={require("./assets/lalang.jpg")} style={{flex: 1, width: undefined, height: undefined}} resizeMode="center"></Image>
          </View>
          <View style={{width: 300, height: 60, marginTop: 20, borderRadius: 3}}>
            <Text style={{alignSelf: "center", color: 'white', fontSize: 24, fontFamily: 'monospace', fontWeight: 'bold'}}>Tegar Saksi Ilalang</Text>
          </View>
        </View>
        <View style={{alignItems: 'center', flex: 1, backgroundColor: '#808080'}}>
          <View style={{flexDirection: 'row', backgroundColor: 'white', width: 360, height: 65, marginTop: -35, borderRadius: 10}}>
            <View style={{width: 120}}>
              <Text style={styles.textboldcenter}>Kelas</Text>
              <Text style={styles.text}>XII</Text>
            </View>
            <View style={{width: 120}}>
              <Text style={styles.textboldcenter}>Jurusan</Text>
              <Text style={styles.text}>RPL-A</Text>
            </View>
            <View style={{width: 120}}>
              <Text style={styles.textboldcenter}>Absen</Text>
              <Text style={styles.text}>30</Text>
            </View>
          </View>
          <View style={{flexDirection: 'row', backgroundColor: 'white', width: 360, height: 306, marginTop: 20, marginBottom: 20, borderRadius: 10}}>
            <View style={{width: 110}}>
              <View style={{height: 65, marginTop: 9}}>
                <Text style={styles.textboldcenter}><Icon name="institution" size={60} /></Text>
              </View>
              <View style={{height: 65, marginTop: 9}}>
                <Text style={styles.textboldcenter}><Icon name="instagram" size={60} /></Text>
              </View>
              <View style={{height: 65, marginTop: 9}}>
                <Text style={styles.textboldcenter}><Icon name="envelope-o" size={60} /></Text>
              </View>
              <View style={{height: 65, marginTop: 9}}>
                <Text style={styles.textboldcenter}><Icon name="home" size={60} /></Text>
              </View>
            </View>
            <View style={{width: 250}}>
              <View style={{height: 65, marginTop: 9}}>
                <Text style={styles.textbold}>Sekolah</Text>
                <Text style={styles.textnocenter}>SMKN 2 Surakarta</Text>
              </View>
              <View style={{height: 65, marginTop: 9}}>
                <Text style={styles.textbold}>Instagram</Text>
                <Text style={styles.textnocenter}>@saksiilalang</Text>
              </View>
              <View style={{height: 65, marginTop: 9}}>
                <Text style={styles.textbold}>E-mail</Text>
                <Text style={styles.textnocenter}>lalang1515@gmail.com</Text>
              </View>
              <View style={{height: 65, marginTop: 9}}>
                <Text style={styles.textbold}>Alamat</Text>
                <Text style={styles.textnocenter}>Boyolali, Jawa Tengah</Text>
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  textbold: {
    fontWeight: '700',
    fontSize: 22, 
    fontFamily: 'monospace',
    color: '#333333'
  },
  textboldcenter: {
    fontWeight: '700',
    fontSize: 22, 
    fontFamily: 'monospace', 
    alignSelf: 'center',
    color: '#333333'
  },
  text: {
    fontSize: 22, 
    fontFamily: 'monospace', 
    alignSelf: 'center'
  },
  textnocenter: {
    fontSize: 18, 
    fontFamily: 'monospace', 
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
});

export default App;
